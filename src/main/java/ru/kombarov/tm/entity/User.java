package ru.kombarov.tm.entity;

import ru.kombarov.tm.enumerated.Role;

public final class User extends AbstractEntity {

    private String login;

    private String password;

    private Role role;

    public User(final Role role) {
        this.role = role;
    }

    public User() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }
}
