package ru.kombarov.tm.api;

import java.util.List;

public interface IRepository<T> {

    List<T> findAll();

    List<T> findAll(final String userId);

    T findOne(final String id);

    T findOne(final String userId, final String id);

    void persist(final T t) throws Exception;

    void merge(final T t) throws Exception;

    void remove(final String id) throws Exception;

    void removeAll();

    void removeAll(final String userId) throws Exception;

    String getIdByName(final String name);
}
