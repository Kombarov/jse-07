package ru.kombarov.tm.command;

import ru.kombarov.tm.api.ServiceLocator;
import ru.kombarov.tm.context.Bootstrap;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    protected Bootstrap bootstrap;

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void setBootstrap(final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;
}
