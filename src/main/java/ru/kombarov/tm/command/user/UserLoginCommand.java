package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.PasswordUtil.generateHash;

public final class UserLoginCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "user-login";
    }

    @Override
    public String description() {
        return "Login.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN");
        String login = input.readLine();
        while (!serviceLocator.getUserService().checkLogin(login)) {
            System.out.println("LOGIN DOES NOT EXIST");
            System.out.println("ENTER LOGIN");
            login = input.readLine();
        }
        System.out.println("[OK]");
        User user = null;
        for (User user1 : serviceLocator.getUserService().findAll()) {
            if (user1.getLogin().equals(login)) user = user1;
        }
        System.out.println("ENTER PASSWORD");
        String password = input.readLine();
        while (!user.getPassword().equals(generateHash(password))) {
            System.out.println("INVALID PASSWORD");
            System.out.println("ENTER PASSWORD");
            password = input.readLine();
        }
        serviceLocator.getUserService().setUserCurrent(user);
        System.out.println("[OK]");
    }
}
