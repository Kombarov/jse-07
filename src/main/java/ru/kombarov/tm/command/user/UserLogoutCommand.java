package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-logout";
    }

    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("USER LOG OUT");
            serviceLocator.getUserService().setUserCurrent(null);
            System.out.println("[OK]");
        }
    }
}
