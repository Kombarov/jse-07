package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.*;

public final class UserViewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-view";
    }

    @Override
    public String description() {
        return "Show user info.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[USER VIEW]");
            printUser(serviceLocator.getUserService().getUserCurrent());
            printProjects(serviceLocator.getProjectService().getProjectsByUserId(serviceLocator.getUserService().getUserCurrent().getId()));
            System.out.println();
            System.out.println("tasks:");
            printTasks(serviceLocator.getTaskService().getTasksByUserId(serviceLocator.getUserService().getUserCurrent().getId()));
            System.out.println("[OK]");
        }
    }
}
