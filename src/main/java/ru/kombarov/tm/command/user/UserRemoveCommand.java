package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.enumerated.Role;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printUsers;

public final class UserRemoveCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "user-remove";
    }

    @Override
    public String description() {
        return "Remove existing user";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent().getRole().equals(Role.ADMINISTRATOR)) {
            System.out.println("[USER REMOVE]");
            printUsers(serviceLocator.getUserService().findAll());
            System.out.println("TYPE USER NAME TO REMOVE");
            final String name = input.readLine();
            serviceLocator.getUserService().remove(serviceLocator.getUserService().getIdByName(name));
            System.out.println("[OK]");
        }
        else System.out.println("ACCESS IS DENIED");
    }
}
