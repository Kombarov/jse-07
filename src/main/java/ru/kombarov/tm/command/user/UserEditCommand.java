package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public final class UserEditCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "user-edit";
    }

    @Override
    public String description() {
        return "Edit user.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[USER EDIT]");
            System.out.println("YOUR USERNAME IS " + serviceLocator.getUserService().getUserCurrent().getLogin());
            System.out.println("ENTER NEW USERNAME");
            String login = input.readLine();
            User user = new User();
            user = serviceLocator.getUserService().getUserCurrent();
            user.setLogin(login);
            serviceLocator.getUserService().setUserCurrent(user);
            serviceLocator.getUserService().merge(user);
            System.out.println("[OK]");
        }
    }
}
