package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Task;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;

public final class TaskCreateCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK CREATE]");
            System.out.println("ENTER TASK NAME");
            final Task task = new Task(input.readLine());
            System.out.println("ENTER TASK DESCRIPTION");
            task.setDescription(input.readLine());
            System.out.println("ENTER START DATE");
            task.setDateStart(parseStringToDate(input.readLine()));
            System.out.println("ENTER FINISH DATE");
            task.setDateFinish(parseStringToDate(input.readLine()));
            task.setUserId(serviceLocator.getUserService().getUserCurrent().getId());
            serviceLocator.getTaskService().persist(task);
            System.out.println("[OK]");
        }
    }
}
