package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskRemoveCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK REMOVE]");
            printTasks(serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
            System.out.println("ENTER TASK NAME FOR REMOVE");
            serviceLocator.getTaskService().remove(serviceLocator.getTaskService().getIdByName(input.readLine()));
            System.out.println("[OK]");
        }
    }
}
