package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Task;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskUnattachCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-unattach";
    }

    @Override
    public String description() {
        return "Unattach task from the project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK UNATTACH]");
            printTasks(serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
            System.out.println("ENTER TASK NAME");
            final String taskName = input.readLine();
            final Task task = serviceLocator.getTaskService().findOne(serviceLocator.getTaskService().getIdByName(taskName));
            task.setProjectId("");
            System.out.println("[OK]");
        }
    }
}
