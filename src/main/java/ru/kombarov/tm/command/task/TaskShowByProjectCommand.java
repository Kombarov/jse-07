package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printProjects;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskShowByProjectCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-show by project";
    }

    @Override
    public String description() {
        return "Show tasks by project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK SHOW BY PROJECT]");
            printProjects(serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
            System.out.println("ENTER PROJECT NAME");
            printTasks(serviceLocator.getTaskService().getTasksByProjectId(serviceLocator.getProjectService().getIdByName(input.readLine())));
            System.out.println("[OK]");
        }
    }
}
