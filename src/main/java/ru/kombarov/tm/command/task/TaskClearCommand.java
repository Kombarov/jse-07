package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK CLEAR]");
            serviceLocator.getTaskService().removeAll(serviceLocator.getUserService().getUserCurrent().getId());
            System.out.println("[OK]");
        }
    }
}
