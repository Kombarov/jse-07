package ru.kombarov.tm.command.project;

import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printProjects;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[PROJECT LIST]");
            printProjects(serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
            System.out.println("[OK]");
        }
    }
}
