package ru.kombarov.tm.command.project;

import ru.kombarov.tm.command.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printProject;
import static ru.kombarov.tm.util.EntityUtil.printProjects;

public final class ProjectSelectCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "project-select";
    }

    @Override
    public String description() {
        return "Select the project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUserService().getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[PROJECT SELECT]");
            printProjects(serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
            System.out.println("ENTER PROJECT NAME");
            printProject(serviceLocator.getProjectService().findOne(serviceLocator.getProjectService().getIdByName(input.readLine())));
            System.out.println("[OK]");
        }
    }
}
