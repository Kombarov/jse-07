package ru.kombarov.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.kombarov.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String command() {
        return "about";
    }

    @Override
    public String description() {
        return "Print build information.";
    }

    @Override
    public void execute() throws Exception {
        String developer = Manifests.read("developer");
        String createdBy = Manifests.read("Created-By");
        String buildJdk = Manifests.read("Build-Jdk");
        String buildNumber = Manifests.read("buildNumber");
        System.out.println("Developer: " + developer);
        System.out.println("Created by: " + createdBy);
        System.out.println("Build JDK: " + buildJdk);
        System.out.println("Build number: " + buildNumber);
    }
}
