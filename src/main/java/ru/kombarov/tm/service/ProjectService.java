package ru.kombarov.tm.service;

import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.repository.AbstractRepository;
import ru.kombarov.tm.repository.ProjectRepository;

import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IRepository<Project> {

    private final ProjectRepository projectRepository = (ProjectRepository) abstractRepository;

    public ProjectService(AbstractRepository<Project> abstractRepository) {
        super(abstractRepository);
    }

    public void persist(final Project project) throws Exception {
        if (project == null) throw new Exception();
        else abstractRepository.persist(project);
    }

    public void merge(final Project project) throws Exception {
        if (project == null) throw new Exception();
        else abstractRepository.merge(project);
    }

    public String getIdByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        else return abstractRepository.getIdByName(name);
    }

    public List<Project> getProjectsByUserId(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.getProjectsByUserId(userId);
    }

    public List<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return abstractRepository.findAll(userId);
    }

    public Project findOne(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return abstractRepository.findOne(userId, id);
    }

    public void removeAll(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        abstractRepository.removeAll(userId);
    }
}