package ru.kombarov.tm.service;

import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.repository.AbstractRepository;
import ru.kombarov.tm.repository.UserRepository;

import java.util.List;

public final class UserService extends AbstractService<User> implements IRepository<User> {

    private User userCurrent;

    private final UserRepository projectRepository = (UserRepository) abstractRepository;

    public UserService(AbstractRepository<User> abstractRepository) {
        super(abstractRepository);
    }

    public void persist(final User user) throws Exception {
        if (user == null) throw new Exception();
        else abstractRepository.persist(user);
    }

    public void merge(final User user) throws Exception {
        if (user == null) throw new Exception();
        else abstractRepository.merge(user);
    }

    public String getIdByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        else return abstractRepository.getIdByName(name);
    }

    public List<User> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return abstractRepository.findAll(userId);
    }

    public User findOne(final String userId, final String id) {
        if (id == null || id.isEmpty()) return null;
        else return abstractRepository.findOne(userId, id);
    }

    public void removeAll(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        abstractRepository.removeAll(userId);
    }

    public void setUserCurrent(final User user) {
        userCurrent = user;
    }

    public User getUserCurrent() {
        return userCurrent;
    }

    public boolean checkLogin(final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        return projectRepository.checkLogin(login);
    }
}