package ru.kombarov.tm.service;

import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.repository.AbstractRepository;
import ru.kombarov.tm.repository.TaskRepository;

import java.util.List;

public final class TaskService extends AbstractService<Task> implements IRepository<Task> {

    private final TaskRepository taskRepository = (TaskRepository) abstractRepository;

    public TaskService(AbstractRepository<Task> abstractRepository) {
        super(abstractRepository);
    }

    public void persist(final Task task) throws Exception {
        if (task == null) throw new Exception();
        else abstractRepository.persist(task);
    }

    public void merge(final Task task) throws Exception {
        if (task == null) throw new Exception();
        else abstractRepository.merge(task);
    }

    public String getIdByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return abstractRepository.getIdByName(name);
    }

    public List<Task> getTasksByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.getTasksByProjectId(projectId);
    }

    public List<Task> getTasksByUserId(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.getTasksByUserId(userId);
    }

    public List<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return abstractRepository.findAll();
    }

    public Task findOne(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return abstractRepository.findOne(userId, id);
    }

    public void removeAll(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        abstractRepository.removeAll(userId);
    }
}