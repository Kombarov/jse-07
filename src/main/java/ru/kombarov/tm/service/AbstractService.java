package ru.kombarov.tm.service;

import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.repository.AbstractRepository;

import java.util.List;

public abstract class AbstractService<T> implements IRepository<T> {

    protected AbstractRepository<T> abstractRepository;

    public AbstractService(final AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Override
    public List<T> findAll() {
        return abstractRepository.findAll();
    }

    @Override
    public T findOne(final String id) {
        if (id == null || id.isEmpty()) return null;
        else return abstractRepository.findOne(id);
    }

    @Override
    public void remove(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        else abstractRepository.remove(id);
    }

    @Override
    public void removeAll() {
        abstractRepository.removeAll();
    }
}