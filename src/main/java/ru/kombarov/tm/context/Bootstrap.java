package ru.kombarov.tm.context;

import ru.kombarov.tm.api.ServiceLocator;
import ru.kombarov.tm.command.*;
import ru.kombarov.tm.command.system.AboutCommand;
import ru.kombarov.tm.command.system.HelpCommand;
import ru.kombarov.tm.command.project.*;
import ru.kombarov.tm.command.task.*;
import ru.kombarov.tm.command.user.*;
import ru.kombarov.tm.repository.ProjectRepository;
import ru.kombarov.tm.repository.TaskRepository;
import ru.kombarov.tm.repository.UserRepository;
import ru.kombarov.tm.service.ProjectService;
import ru.kombarov.tm.service.TaskService;
import ru.kombarov.tm.service.UserService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final UserService userService = new UserService(userRepository);

    private final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        registry(new HelpCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectEditCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveCommand());
        registry(new ProjectSelectCommand());
        registry(new TaskAttachCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskEditCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveCommand());
        registry(new TaskSelectCommand());
        registry(new TaskShowByProjectCommand());
        registry(new TaskUnattachCommand());
        registry(new UserCreateCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserEditCommand());
        registry(new UserViewCommand());
        registry(new UserRemoveCommand());
        registry(new AboutCommand());
        String command = "";
        while (!"exit".equals(command)) {
            command = input.readLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    public void registry(final AbstractCommand command) throws Exception {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new Exception();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new Exception();
        command.setServiceLocator(this);
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserService getUserService() {
        return userService;
    }
}