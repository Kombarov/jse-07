package ru.kombarov.tm.repository;

import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IRepository<User> {

    public void persist(final User user) {
        entityMap.put(user.getId(), user);
    }

    public void merge(final User user) {
        if (entityMap.containsKey(user.getId())) {
            entityMap.remove(user.getId());
            entityMap.put(user.getId(), user);
        }
        else {
            entityMap.put(user.getId(), user);
        }
    }

    public List<User> findAll(final String userId) {
        final List<User> list = new ArrayList<User>(entityMap.values());
        return list;
    }

    public User findOne(final String userId, final String id) {
        return entityMap.get(id);
    }

    public void removeAll(final String userId) {
        entityMap.clear();
    }

    public String getIdByName(final String name) {
        final List<User> users = findAll();
        for (final User user : users) {
            if (user.getLogin().equals(name)) return user.getId();
        }
        return null;
    }

    public boolean checkLogin(final String login) {
        for (User user : findAll()) {
            if (user.getLogin().equals(login)) return true;
        }
        return false;
    }
}
