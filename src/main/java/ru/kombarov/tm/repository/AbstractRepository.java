package ru.kombarov.tm.repository;

import ru.kombarov.tm.api.IRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T> implements IRepository<T> {

    protected Map<String, T> entityMap = new HashMap<>();

    @Override
    public List<T> findAll() {
        final List<T> list = new ArrayList<T>(entityMap.values());
        return list;
    }

    @Override
    public T findOne(final String id) {
        return entityMap.get(id);
    }

    @Override
    public void remove(final String id) throws Exception {
        entityMap.remove(id);
    }

    @Override
    public void removeAll() {
        entityMap.clear();
    }
}
