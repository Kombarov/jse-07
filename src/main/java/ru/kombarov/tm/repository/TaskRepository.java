package ru.kombarov.tm.repository;

import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements IRepository<Task> {

    public void persist(final Task task) {
        entityMap.put(task.getId(), task);
    }

    public void merge(final Task task) {
        if (entityMap.containsKey(task.getId())) {
            entityMap.remove(task.getId());
            entityMap.put(task.getId(), task);
        }
        else {
            entityMap.put(task.getId(), task);
        }
    }

    public List<Task> findAll(final String userId) {
        final List<Task> list = new ArrayList<Task>();
        for (final Task task : findAll()) {
            if (task.getUserId().equals(userId)) list.add(task);
        }
        return list;
    }

    public Task findOne(final String userId, final String id) {
        for (Task task : findAll(userId)) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public void removeAll(final String userId) throws Exception {
        for (final Task task : findAll()) {
            if (task.getUserId().equals(userId)) remove(task.getId());
        }
    }

    public String getIdByName(final String name) {
        final List<Task> tasks = findAll();
        for (final Task task : tasks) {
            if (task.getName().equals(name)) return task.getId();
        }
        return null;
    }

    public List<Task> getTasksByProjectId(final String projectId) {
        final List<Task> list = findAll();
        final List<Task> sortedList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getProjectId().equals(projectId)) sortedList.add(list.get(i));
        }
        return sortedList;
    }

    public List<Task> getTasksByUserId(final String userId) {
        final List<Task> list = findAll();
        final List<Task> sortedList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getUserId().equals(userId)) sortedList.add(list.get(i));
        }
        return sortedList;
    }
}
