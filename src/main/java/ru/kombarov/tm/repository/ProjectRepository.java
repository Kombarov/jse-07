package ru.kombarov.tm.repository;

import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IRepository<Project> {

    public void persist(final Project project) {
        entityMap.put(project.getId(), project);
    }

    public void merge(final Project project) {
        if (entityMap.containsKey(project.getId())) {
            entityMap.remove(project.getId());
            entityMap.put(project.getId(), project);
        }
        else {
            entityMap.put(project.getId(), project);
        }
    }

    public void removeAll() {
        entityMap.clear();
    }

    public List<Project> findAll(final String userId) {
        final List<Project> list = new ArrayList<Project>();
        for (final Project project : findAll()) {
            if (project.getUserId().equals(userId)) list.add(project);
        }
        return list;
    }

    public Project findOne(final String userId, final String id) {
        for (final Project project : findAll(userId)) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public void removeAll(final String userId) throws Exception {
        for (final Project project : findAll()) {
            if (project.getUserId().equals(userId)) remove(project.getId());
        }
    }

    public String getIdByName(final String name) {
        final List<Project> projects = findAll();
        for (final Project project : projects) {
            if (project.getName().equals(name)) return project.getId();
        }
        return null;
    }

    public List<Project> getProjectsByUserId(final String userId) {
        final List<Project> list = findAll();
        final List<Project> sortedList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getUserId().equals(userId)) sortedList.add(list.get(i));
        }
        return sortedList;
    }
}