# Task Manager
Software requirements:
  - JRE 11
  - Apache Maven 3.6.0


### Developer
Developer of this app is Andrey Kombarov

e-mail: kombarovandrew@yandex.ru

### Commands for building the application

```sh
mvn clean
mvn install
```
### To run the application
```sh
java -jar task-manager-3.0.jar
```
